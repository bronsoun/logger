from flask import Flask, render_template, request, session, logging, url_for, redirect, flash
from sqlalchemy import create_engine
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import scoped_session, sessionmaker

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///logger.db'

db = SQLAlchemy(app)

class logger(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    cookie = db.Column(db.String(200), unique = True, nullable = True)
    referer = db.Column(db.String(200), unique = True, nullable = True)
    useragent = db.Column(db.String(200), unique = True)
    ip = db.Column(db.String(200), unique = True)
    accept = db.Column(db.String(200), unique = True)
    



@app.route("/", methods = ["POST", "GET"])
def home():
    if request.method == "GET":
        try:
            useragent = request.headers.get('User-Agent')
            referer = request.headers.get('Referer')
            cookie = request.headers.get('Cookie')
            ip = request.remote_addr
            accept = request.headers.get("Accept")
        
         
            data = logger(useragent = useragent, referer = referer, cookie = cookie, ip = ip, accept = accept)
            db.session.add(data)
            db.session.flush()
            db.session.commit()
        except:
            db.session.rollback()

        
    return render_template("redirecting.html")


if __name__ == "__main__":
    #when we run app firstly, we need to "db.create_all()"
    app.run(host = '0.0.0.0', debug = True)